extern crate mako;

use mako;

/// Examples herein are far from final, and are mostly here just for my
/// reference during the development process. That said, they should
/// roughly represent the basic idea of what we're going for.
fn main() {
    /// Login can be achieved using either `.with_login` or `.with_token`.
    /// Most users will likely want to use their login_id and password,
    /// so the with_login function will be the more common use-case.
    let maco = Maco::new("https://chat.pop-os.org", Authentication:cred("myemail@pop-planet.info", "my_super_secret_password"));
}