//! # Maco
//!
//! Maco is a simple Mattermost API Connector in Rust. It is being written
//! for my upcoming GTK3 Mattermost client, but it is intended to be flexible
//! enough to be reused. Feel free to use it, extend it, or whatever!
//!
//! Let me know if/how you use it, I'd love to see what people come up with!
//!
//! For examples, see the `examples` directory
//!
//! # Contributing
//!
//! Anyone is welcome to contribute. Please read the [guidelines for contributing](
//! https://gitlab.com/pop-planet/maco/blob/master/CONTRIBUTING.md).

pub enum Authentication<'a> {
    Credentials { login_id: &'a str, password: &'a str },
    Token { token: &'a str }
}

impl<'a> Authentication<'a> {
    /// Login to the Mattermost API can be achieved by both login_id (email address)
    /// and password, or what Mattermost calls a "Personal Access Token." Using the
    /// builder pattern allows us to let users use either method.
    ///
    /// Once a user is logged in with a login_id and password, Mattermost returns
    /// a "Session Token" which is used in subsequent API calls in the same way
    /// as a Personal Access Token would be.
    pub fn cred(login_id: &'a str, password: &'a str) -> Self {
        Authentication::Credentials { login_id: login_id, password: password }
    }

    pub fn token(token: &'a str) -> Self {
        Authentication::Token { token: token }
    }
}

struct Maco<'a> {
    base_uri: &'a str,
    authentication: Authentication<'a>
}

impl<'a> Maco<'a> {
    pub fn new(base_uri: &'a str, authentication: Authentication<'a> ) -> Self {
        Maco { base_uri: base_uri, authentication: authentication }
    }

    /// Given the base URI for the server and an API method, construct the
    /// API endpoint for a specific call.
    fn get_uri_for_method(&self, method: &'a str) -> String {
        [self.base_uri, "/api/v4/", method].concat()
    }
}
